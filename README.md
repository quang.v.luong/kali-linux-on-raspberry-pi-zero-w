# kali linux on raspberry pi zero w

## Prerequisites and Assumptions
* Raspberry Pi Zero W
* MicroSD Card (8GB or greater)
* MicroSD to SD Card adapter (if applicable)
* Micro USB Cable
* Windows 10 Pro
* Kali Linux arm image for raspberry pi zero w image from [https://www.offensive-security.com/kali-linux-arm-images/](https://www.offensive-security.com/kali-linux-arm-images/)
* balenaEtcher for flashing the image to the microsd card from [https://www.balena.io/etcher/](https://www.balena.io/etcher/)